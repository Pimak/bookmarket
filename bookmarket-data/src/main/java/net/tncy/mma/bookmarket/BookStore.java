package net.tncy.mma.bookmarket;

import java.util.ArrayList;
import java.util.List;


public class BookStore {

    private static int nextId = 0;

    private int id;
    private String name;
    private List<InventoryEntry> inventoryEntries;

    public BookStore(String name) {
        this.id = ++nextId;
        this.name = name;
        this.inventoryEntries = new ArrayList<>();
    }

    public void addInventoryEntry(InventoryEntry inventoryEntry){
        this.inventoryEntries.add(inventoryEntry);
    }

    public InventoryEntry getInventoryEntry(Book book){
        for(InventoryEntry inventoryEntry : this.inventoryEntries){
            if (inventoryEntry.getBook()==book){
                return inventoryEntry;
            }
        }
        return null;
    }
}
