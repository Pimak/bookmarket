package net.tncy.mma.bookmarket;

import net.tncy.mma.validator.ISBN;

public class Book {

    private static int nextId=0;

    private int id;
    private String title;
    private String author;
    @ISBN
    private String isbn;
    private BookFormat format;

    public Book(String title, String author, String isbn, BookFormat format) {
        this.id = ++nextId;
        this.title = title;
        this.author = author;
        this.isbn=isbn;
        this.format=format;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public BookFormat getFormat() {
        return format;
    }

    public void setFormat(BookFormat format) {
        this.format = format;
    }
}
